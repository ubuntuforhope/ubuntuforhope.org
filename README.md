This repository holds the Nikola files for the [ubuntuforhope.org](https://ubuntuforhope.org/) website. These are the raw files needed by Nikola to build the static web pages - so you need to install Nikola to help with contributions.

## Nikola install
Run the following in terminal for the first time setup (uses python3, and assumes you are using Ubuntu)

```
sudo apt install virtualenv python3
cd ~/
virtualenv -p python3 nikola
cd nikola
source bin/activate
pip install --upgrade "Nikola[extras]"
```

## Get theme ready
The theme uses Bootstrap4 and its Sass files to get the maximum amount of control( eg. a lot of bootstrap components are left out to decrease the css file as much as possible). So the following is needed to install Sass, have Nikola compile it and to get the theme's asset files.
```
sudo apt install ruby npm
sudo su -c "gem install sass"
nikola plugin --install=sass
cd themes/bootstrap4/sass/
npm install
```

## Building the web pages and test output
The web pages can now be build using

```
nikola build
nikola serve
```

Then point your browser to localhost:8000

## Making and testing changes
When you want to make changes it might be quickest to have Nikola watch the files and auto rebuild as changes occur. To do this run the following and the browser will auto reload as posts are edited and saved.

```
nikola auto
```

### Nikola not installed error
If you already installed Nikola and get the following when you start a new terminal session, then run the following.

```
source ~/nikola/bin/activate
```

You should now see `(nikola)` prepended to the command prompt.

## Committing changes
These are the same as any Git repository (create a branch, commit the changes, push the branch and create a merge request). If you have any issues, ask for on [IRC (Freenode) at #ubuntu-za](https://ubuntu-za.org/irc.html).
