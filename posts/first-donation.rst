.. title: The first ones
.. slug: first-ones
.. date: 2016-10-06 08:20:24 UTC+02:00
.. tags: Donations, Restoration
.. category:
.. link:
.. description: Short story of the first PCs that we restored
.. type: text
.. previewimage: images/Donation-Jun-2016.jpg

Early in June we received our first donation. Karl has already gotten them ready by installing Ubuntu Mate on them during the past few months. Now we are expecting their collection any day.

.. raw:: html

    <figure class="figure">
        <img src="/images/Donation-Jun-2016.jpg" class="figure-img img-fluid img-rounded" alt="The three PC we received as donations">
        <figcaption class="figure-caption text-xs-center">Three PCs we received as donations</figcaption>
    </figure>
