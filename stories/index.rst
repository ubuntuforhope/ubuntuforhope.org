.. title: It's nice to be nice
.. hidetitle: True
.. slug: index
.. date: 2016-07-29 21:01:34 UTC+02:00
.. tags:
.. category:
.. link:
.. description: A community project which aims to take old PC, restored them, and gives them away to students and people in need
.. type: text

.. raw:: html

    <div class="row">
        <div class="col-sm-12 jumbotron">
            <p>Want to stay up to date with our latest activities?
            Then follow us on social media.</p>

            <a href="https://www.facebook.com/Ubuntu-for-Hope-1117317621653050/" class="btn btn-outline-info btn-lg m-r-1">Facebook</a>
            <a href="https://twitter.com/ubuntuhope" class="btn btn-outline-info btn-lg m-r-1">Twitter</a>
            <a href="/rss.xml" class="btn btn-outline-secondary btn-lg">RSS Feed</a>
        </div>
    </div>

.. raw:: html

    <div class="row text-xs-center">
        <div class="col-sm-4">
            <i class="fa fa-5x fa-arrow-down bg-primary p-a-2 m-y-2 img-circle"></i>
            <h3>From Old</h3>
            <p>We aim to take donated or refurbished PC's - of any age, size, and condition. These can come from any location, whether it is just that one PC that you have been storing for the last four or so years, or it maybe a few workstations that has been replaced with new ones.</p>
        </div>
        <div class="col-sm-4">
            <i class="fa fa-5x fa-users bg-primary p-a-2 m-y-2 img-circle"></i>
            <h3>Restoration</h3>
            <p>Ubuntu is then installed on them with the help of the <a href="http://ubuntu-za.org" target="_blank">Ubuntu-za Loco</a> community and helpful volunteers. We are currently using Ubuntu Mate specifically due to its low system requirements.</p>
        </div>
        <div class="col-sm-4">
            <i class="fa fa-5x fa-arrow-up bg-primary p-a-2 m-y-2 img-circle"></i>
            <h3>Give Away</h3>
            <p>These restored or freshly cleaned PC are then donated to students and people in need. Since we are a community project, there will never be any exchange of money.</p>
        </div>
    </div>
